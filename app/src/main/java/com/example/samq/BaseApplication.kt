package com.example.samq

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
public class BaseApplication : Application()