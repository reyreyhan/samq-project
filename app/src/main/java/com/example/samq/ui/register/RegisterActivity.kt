package com.example.samq.ui.register

import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.TypedValue
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.samq.R
import com.example.samq.util.dp
import com.google.android.material.bottomsheet.BottomSheetBehavior
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.bottom_sheet_register_help_layout.*
import kotlinx.android.synthetic.main.bottom_sheet_register_help_layout.view.*

@AndroidEntryPoint
class RegisterActivity : AppCompatActivity() {

    private lateinit var bottomSheetBehavior:BottomSheetBehavior<ConstraintLayout>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        initActionBar()
        initSpinner()
        initBottomSheet()
    }

    private fun initActionBar(){
        //Get Action Bar
        val actionbar = supportActionBar
        //Set Actionbar Title
        actionbar!!.title = "Pendaftaran Merchant QRIS"
        //Set Back Button
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)
    }

    private fun initSpinner() {
        val listCriteria = mutableListOf(
            getString(R.string.activity_register_spinner_merchant_business_hint),
            "Usaha Mikro",
            "Usaha Kecil",
            "Usaha Menengah",
            "Usaha Besar"
        )

        val listBusiness = mutableListOf(
            getString(R.string.activity_register_spinner_merchant_criteria_hint),
            "1",
            "2",
            "3",
            "4"
        )

        val adapterBusiness: ArrayAdapter<String> = object : ArrayAdapter<String>(
            this,
            android.R.layout.simple_spinner_dropdown_item,
            listBusiness
        ) {
            override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                val view = super.getView(position, convertView, parent) as TextView
                view.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
                view.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f)
                view.setTextColor(Color.GRAY)
                return view
            }

            override fun getDropDownView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                val view: TextView = super.getDropDownView(
                    position,
                    convertView,
                    parent
                ) as TextView
                view.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
                view.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15f)
                view.setPadding(35.dp, 20.dp, 35.dp, 20.dp)
                if (position == spinner_merchant_business.selectedItemPosition && position != 0) {
                    view.background = ColorDrawable(Color.parseColor("#F7E7CE"))
                    view.setTextColor(Color.parseColor("#333399"))
                }

                if (position == 0) {
                    view.setTextColor(Color.GRAY)
                }

                return view
            }

            override fun isEnabled(position: Int): Boolean {
                return position != 0
            }
        }

        val adapterCriteria: ArrayAdapter<String> = object : ArrayAdapter<String>(
            this,
            android.R.layout.simple_spinner_dropdown_item,
            listCriteria
        ) {
            override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                val view = super.getView(position, convertView, parent) as TextView
                view.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
                view.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f)
                view.setTextColor(Color.GRAY)
                return view
            }

            override fun getDropDownView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                val view: TextView = super.getDropDownView(
                    position,
                    convertView,
                    parent
                ) as TextView
                view.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
                view.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15f)
                view.setPadding(35.dp, 20.dp, 35.dp, 20.dp)
                // set selected item style
                if (position == spinner_merchant_criteria.selectedItemPosition && position != 0) {
                    view.background = ColorDrawable(Color.parseColor("#F7E7CE"))
                    view.setTextColor(Color.parseColor("#333399"))
                }

                // make hint item color gray
                if (position == 0) {
                    view.setTextColor(Color.GRAY)
                }

                return view
            }

            override fun isEnabled(position: Int): Boolean {
                return position != 0
            }
        }

        spinner_merchant_business.adapter = adapterBusiness
        spinner_merchant_criteria.adapter = adapterCriteria
    }

    private fun initBottomSheet() {
        val listOfHelp = mutableListOf(
            "<b>Usaha Mikro</b> memiliki Omzet senilai <b>maksimal 300 Juta</b>",
            "<b>Usaha Kecil</b> memiliki Omzet senilai <b>maksimal 300 Juta sampai 2,5 Milyar</b>",
            "<b>Usaha Menengah</b> memiliki Omzet senilai <b>maksimal 2,5 Milyar sampai 10 Milyar</b>",
            "<b>Usaha Besar</b> memiliki Omzet senilai <b>di atas 10 Milyar</b>"
        )
        lbl_criteria.setOnClickListener {
            slideUpDownBottomSheet()
        }
        bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet)
        bottomSheetBehavior.setBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                    }
                    BottomSheetBehavior.STATE_HIDDEN -> {

                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {

                    }
                    BottomSheetBehavior.STATE_SETTLING -> {

                    }
                }
            }
        })
        bottom_sheet.rv_help.adapter = HelpBottomSheetAdapter(listOfHelp.toList())
        bottom_sheet.rv_help.layoutManager = LinearLayoutManager(this)
}

    private fun slideUpDownBottomSheet() {
        if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        } else {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        }
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            if (bottomSheetBehavior.state === BottomSheetBehavior.STATE_EXPANDED) {
                val outRect = Rect()
                bottom_sheet.getGlobalVisibleRect(outRect)
                if (!outRect.contains(
                        event.rawX.toInt(),
                        event.rawY.toInt()
                    )
                ) bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }
        }
        return super.dispatchTouchEvent(event)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}