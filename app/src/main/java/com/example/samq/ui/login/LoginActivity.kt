package com.example.samq.ui.login

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.samq.R
import com.example.samq.ui.register.RegisterActivity
import com.google.android.material.tabs.TabLayout
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_login.*

@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setRegisterLabelText()
        tab_login.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener{
            override fun onTabSelected(p0: TabLayout.Tab?) {
                switchLoginTab(p0!!.position)
            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {

            }

            override fun onTabReselected(p0: TabLayout.Tab?) {

            }
        })
    }

    private fun setRegisterLabelText(){
        val ss = SpannableString(getString(R.string.login_activity_lbl_register))
        val clickableSpan: ClickableSpan = object : ClickableSpan() {
            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
            }

            override fun onClick(p0: View) {
                startActivity(Intent(this@LoginActivity, RegisterActivity::class.java))
            }
        }
        ss.setSpan(clickableSpan, 21, ss.length-1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        lbl_register.text = ss
        lbl_register.movementMethod = LinkMovementMethod.getInstance()
        lbl_register.highlightColor = Color.TRANSPARENT
    }

    fun switchLoginTab(selected:Int){
        when(selected){
            0 ->{
                lbl_et_pin.text = "PIN Owner"
                lbl_register.visibility = View.VISIBLE
            }
            1 ->{
                lbl_et_pin.text = "PIN Kasir"
                lbl_register.visibility = View.GONE
            }
        }
    }
}